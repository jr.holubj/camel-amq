package com.honza.amqapp.config;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

import javax.jms.JMSException;

@Configuration
@EnableJms
@EnableAutoConfiguration
public class ArtemisConfig {

    @Bean
    public JmsComponent jmsComponent(
            @Value("${spring.artemis.host}") final String host,
            @Value("${spring.artemis.port}") final String port,
            @Value("${spring.artemis.user}") final String user,
            @Value("${spring.artemis.password}") final String password
    ) throws JMSException {
        // Create the connectionfactory which will be used to connect to Artemis
        ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
        cf.setBrokerURL("tcp://" + host + ":" + port);
        cf.setUser(user);
        cf.setPassword(password);

        // Create the Camel JMS component and wire it to our Artemis connectionfactory
        JmsComponent jms = new JmsComponent();
        jms.setConnectionFactory(cf);
        return jms;
    }
}
