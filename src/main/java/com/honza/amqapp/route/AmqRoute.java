package com.honza.amqapp.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class AmqRoute extends RouteBuilder {

    public static int counter = 0;

    @Override
    public void configure() throws Exception {
        from("{{amq.queue}}")
                .log("Received message from Artemis queue: ${body}");

        from("{{amq.topic}}")
                .log("Received message from Artemis topic: ${body}");

        from("timer://sendToMq?period=5000")
                .log("Timer: sending message to topic")
                .setBody(exchange -> {
                    counter++;
                    return "Hello artemis!!! " + counter;
                })
                .to("{{amq.topic");
    }
}
